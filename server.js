const jsonServer = require('json-server')

const server = jsonServer.create()
const middlewares = jsonServer.defaults()
const router = jsonServer.router('./db.json')
const port = process.env.PORT || 3001

server.use(middlewares)
server.use(jsonServer.rewriter({
  '/api/*': '/$1',
}))

server.get('/temperature/:machine_id', (req, res) => {
  const { machine_id } = req.params
  if (machine_id) {
    const { db } = router
    const machine = db.get('machines').find(item => item.machine_id === machine_id)
    if (machine) {
      // return 30 most recent readings
      const temps = machine.get('temps')
      const response = temps.slice(temps.size() - 30).value()
      res.status(200).jsonp({ temps: response })
    } else {
      res.status(400).jsonp({ error: 'No results found' })
    }
  } else {
    res.status(400).jsonp({ error: 'Invalid machine id' })
  }
})

server.use(jsonServer.bodyParser)
server.post('/temperature', (req, res) => {
  const { body } = req
  const { machine_id, timestamp, temperature } = body
  if (machine_id) {
    const { db } = router
    const machine = db.get('machines').find(item => item.machine_id === machine_id)
    if (machine) {
      // add temperature reading to temps collection
      machine.get('temps')
        .push({ timestamp, temp: temperature })
        .write()
      res.status(200).jsonp({ success: 'Temperature submitted successfully' })
    } else {
      res.status(400).jsonp({ error: 'No results found' })
    }
  } else {
    res.status(400).jsonp({ error: 'Invalid machine id' })
  }
})

server.post('/lowstockalert', (req, res) => {
  const { body } = req
  const { machine_id, timestamp, stock } = body
  if (machine_id) {
    const { db } = router
    const machine = db.get('machines').find(item => item.machine_id === machine_id)
    if (machine) {
      // add stock update to stock collection
      machine.set('stock', stock).write()
      res.status(200).jsonp({ success: 'Stock level submitted successfully' })
    } else {
      res.status(400).jsonp({ error: 'No results found' })
    }
  } else {
    res.status(400).jsonp({ error: 'Invalid machine id' })
  }
})

server.use(router)
server.listen(port)